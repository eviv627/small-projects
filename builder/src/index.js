import React from "react";
import ReactDOM from "react-dom";

import "./styles.css";
import "./component/Header.css";

const Header = () => <header className="navbar">Some header</header>;

const MainContent = () => <main>Some main</main>;

const Footer = () => <footer>Some footer</footer>;

function App() {
  return (
    <div className="App">
      <Header />
      <MainContent />
      <Footer />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
