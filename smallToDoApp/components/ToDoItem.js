import React from "react";

const ToDoItem = ({ task }) => (
  <label className="container">
    {" "}
    {task.text}
    <input type="checkbox" checked={task.checked} />
    <span className="checkmark" />
  </label>
);

export default ToDoItem;
