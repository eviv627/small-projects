import React from "react";
import ReactDOM from "react-dom";

import App from "./App.js";
import todosData from "./components/data.js";

import "./styles.css";

const rootElement = document.getElementById("root");

ReactDOM.render(<App tasks={todosData} />, rootElement);
