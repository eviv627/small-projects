import React from "react";
import ToDoItem from "./components/ToDoItem";

import "./App.css";

const App = ({ tasks }) => (
  <div>
    <ul>
      {tasks.map(element => {
        return <ToDoItem task={element} />;
      })}
    </ul>
  </div>
);

export default App;
