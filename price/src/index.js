import React from "react";
import ReactDOM from "react-dom";

import "./styles.css";
import products from "./data.js";

const Product = ({ product }) => {
  return (
    <div>
      <p>id: {product.id}</p>
      <p>name: {product.name}</p>
      <p>price: {product.price}</p>
      <hr />
    </div>
  );
};

function App() {
  return (
    <div>
      {products.map(element => (
        <Product product={element} />
      ))}
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
