import React from "react";
import ReactDOM from "react-dom";

import "./styles.css";

import App from "./App";

const rootElement = document.getElementById("root");

const MultiApp = () => (
  <div>
    <App />
    <App isLoggedIn={true} />
    <App />
  </div>
);

ReactDOM.render(<MultiApp />, rootElement);
