import React from "react"

const state = { 
    name: "%username%",
    isLoggedIn: false 
}

const App = ( { isLoggedIn } ) => {
    if(isLoggedIn === true) {
        state.isLoggedIn = true;
    } else {
       state.isLoggedIn = false;
    }

    return (
        <div>
            <h3>{state.name}</h3>
            <h3>You are currently logged {state.isLoggedIn ? "in" : "out"}</h3>
        </div>
    );
}

export default App